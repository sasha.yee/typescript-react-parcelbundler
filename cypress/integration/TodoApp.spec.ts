/// <reference types="cypress" />

describe("TodoApp tests", () => {
    before(() => {
        cy.visit("/");
    });

    context("The Todo App", () => {
        const todoActionTexts = [
            "This is an automatic test",
            "This item will be marked as complete.",
            "This item will be not marked as complete.",
            "This item is another item that will just exist on the page.",
        ];

        it("The user should be able to interact with the app and have their work saved", () => {
            cy.get("input#todo-add-action").type(todoActionTexts[0]);
            cy.get("form").submit();

            cy.get(".todo-item").find(`input[value="This is an automatic test"]`);

            cy.get(".todo-item").should("have.length", 1);
            cy.get(".todo-item").should("not.have.class", "complete");
            cy
                .get(".todo-item")
                .find("input[type='checkbox']")
                .check();
            cy.get(".todo-item").should("have.class", "complete");
            cy
                .get(".todo-item")
                .find("input[type='checkbox']")
                .uncheck();
            cy.get(".todo-item").should("not.have.class", "complete");

            const otherTodoActionTexts = todoActionTexts.slice(1);

            for (const action of otherTodoActionTexts) {
                cy.get("input#todo-add-action").type(`${action}{enter}`);
                cy.get('.todo-item:first-of-type input[type="text"]').should("have.value", action);
            }

            cy.get(".todo-item").should("have.length", 4);

            cy.get(".todo-item").find(`input[value="${otherTodoActionTexts[0]}"]`);

            cy
                .get(".todo-item")
                .find(`input[value="${otherTodoActionTexts[0]}"]`)
                .closest(".todo-item")
                .find("input[type='checkbox']")
                .check();

            cy
                .get(".todo-item")
                .find(`input[value="${otherTodoActionTexts[0]}"]`)
                .closest(".todo-item")
                .should("have.class", "complete");

            cy
                .get(".todo-item")
                .not(".complete")
                .should("have.length", todoActionTexts.length - 1);

            cy.reload();

            for (const action of todoActionTexts) {
                cy.get(".todo-item").find(`input[value="${action}"]`);
            }

            cy
                .get(".todo-item")
                .find(`input[value="${otherTodoActionTexts[0]}"]`)
                .closest(".todo-item")
                .should("have.class", "complete");

            cy
                .get(".todo-item")
                .not(".complete")
                .should("have.length", todoActionTexts.length - 1);

            cy.clearLocalStorage();
        });
    });
});

describe("Todo validation", () => {
    before(() => {
        cy.visit("/");
    });

    context("You should be able to modify items", () => {
        it("Shouldn't allow the user to add a blank todo item", () => {
            cy.get("form").submit();

            cy.get(".todo-error-message").should("have.length", 1);

            cy.get(".todo-item").should("have.length", 0);

            cy.get("input#todo-add-action").type(" ");

            cy.get(".todo-error-message").should("have.length", 0);
        });
    });
});

describe("Modifying todo items", () => {
    before(() => {
        cy.visit("/");
    });

    context("You should be able to modify items", () => {
        const todoActionTexts = [
            "This is a constant",
            "This one will be modified",
            "This one will be modified and marked as completed",
            "This is marked as completed",
        ];

        it("Should allow users to modify the contents of their todo item", () => {
            for (const action of todoActionTexts) {
                cy.get("input#todo-add-action").type(`${action}{enter}`);
            }

            const updatedAction1 = "This is the newly modified todo item";
            cy
                .get(".todo-item")
                .find(`input[value="${todoActionTexts[1]}"]`)
                .type("{selectall}" + updatedAction1)
                .should("have.value", updatedAction1);

            const updatedAction2 = "This is the newly modified todo item which is completed";
            cy
                .get(".todo-item")
                .find(`input[value="${todoActionTexts[2]}"]`)
                .closest(".todo-item")
                .find('input[type="checkbox"]')
                .check()
                .closest(".todo-item")
                .find(`input[value="${todoActionTexts[2]}"]`)
                .type("{selectall}" + updatedAction2, { force: true })
                .should("have.value", updatedAction2);

            cy
                .get(".todo-item")
                .find(`input[value="${todoActionTexts[3]}"]`)
                .closest(".todo-item")
                .find('input[type="checkbox"]')
                .check()
                .closest(".todo-item")
                .should("have.class", "complete");
        });
    });
});
