# Sasha's Typescript, React & Prettier Boilerplate

An opinionated starting point for developing a Typescript React project utilising Parcel.js for the bundler.

This project was conceived due to the following requirements:

1. I really dislike Webpack, and Parcel looked like a less complex means of bundling a project.
1. A desire to understand how React works and how all of it's pieces fit together.
1. The need to put different means of testing together to allow for confidence that changes to the project won't unintentionally break things.
1. A hatred of automatic changes being made to a codebase when they're not desired.

## Major Components
To keep things as simple as possible this project only has four major parts.

1. Typescript
1. Parcel
1. React
1. Husky

With these come the various supporting libraries, such as `jest` for testing, but these can easily be removed or replaced.

### Typescript
Having a strong typing system available makes for confident development. I much prefer being told by the compiler that something in the code isn't right rather than having clients or QA tell me.

### Parcel
I despise Webpack. It's complex and difficult to understand. To me the bundler should be something that sits in the background quietly and does what I need it to without copious amounts of configuration. Parcel seems to fit those requirements.

### React
One of the most famous front-end libraries, I chose this as it's used at work and I would like to get a better familiarity of how it all works.

### Husky
This is used as a means of easily setting up and maintaining git hooks across different instances of this repository, so that multiple contributors can be sure that the work they're adding to the project all go through the same set of tests.

## Getting started
Following these steps will get you to a point where you can start making use of this boilerplate project.
Replace any instances of text wrapped with square brackets (`[` and `]`) an appropriate value. The text inside the brackets will give an indication of what is required.
e.g. `[new project folder]` would indicate the name of the folder you wish to create your new project within.

Clone this project
```
git clone git@gitlab.com:sasha.yee/typescript-react-parcelbundler.git [new project folder]
```

Update the contents of the `package.json` file, paying particular attention to the `name`, `version`, `description`, `author` and `license`. You should also look at the various different `scripts` to understand what commands are available to be run and what happens when the `pre-commit` and `pre-push` hooks are done. 

## Creating components 
Components can be added in the `./src/components/` folder.

## Testing
Currently only unit and snapshot testing is done for `SampleHeader` component. Eventually visual regression testing will also be added.

## Git hooks
As a means of not-so-subtly reminding the contributor to the repository that there is a standard that should be upheld there are a couple of git hooks that are made use of to prevent code that isn't of a certain quality from being added to the respository.

The following steps are run on the code at various times:

1. prettification using `prettier`
1. linting using `tslint`
1. compiling the code using `tsc`
1. unit testing

Depending on the action that the contributor is performing, these checks will be done on either the files that are changed (`pre-commit`) or the entire project (`pre-push`).

### Stashing before pushing

Since the entire project is checked before being pushed it's a good idea to ensure that the project state is clean.

This can be done by running the command `git stash -u` to stash all files not tracked in the repository.

The files can then be retrieved by running `git stash pop` or `git stash apply`, depending if you want to remove the stash (`pop`) after restoring the files or keeping it (`apply`).

Care should be taken as apparently this [may delete ignored files](https://stackoverflow.com/a/835561) but my own personal testing has shown that with `git 2.16.2` files were not lost.
