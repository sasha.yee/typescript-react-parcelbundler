/// <reference types="cypress" />
import TodoDataStore from "../TodoDataStore";

const todoDataStore = new TodoDataStore();

const time = new Date().getTime();
Date.prototype.getTime = jest.fn().mockReturnValue(time);

afterEach(() => {
    localStorage.clear();
});

test("The Todo Data Store should save an item when one is added", () => {
    todoDataStore.addTodoItem("First item that is being added");
    expect(todoDataStore.getTodoItems()).toMatchObject([
        {
            index: 1,
            action: "First item that is being added",
            completed: false,
        },
    ]);
});

test("The Todo Data Store should save another item and have both stored", () => {
    todoDataStore.addTodoItem("Second item added into the list");
    expect(todoDataStore.getTodoItems()).toMatchObject([
        {
            index: 2,
            action: "Second item added into the list",
            completed: false,
        },
        {
            index: 1,
            action: "First item that is being added",
            completed: false,
        },
    ]);
});

test("Clearing the Todo Data Store should result in the items no longer existing", () => {
    todoDataStore.clearTodoItems();

    expect(todoDataStore.getTodoItems()).toMatchObject([]);
});

test("Adding another item after clearing should result in a store with a single item", () => {
    expect(
        todoDataStore.addTodoItem("Add a test to ensure that a new item in a cleared store is the only item"),
    ).toMatchObject([
        {
            index: 1,
            action: "Add a test to ensure that a new item in a cleared store is the only item",
            completed: false,
        },
    ]);
});

test("Marking an item as complete should show the time", () => {
    const todoItem = todoDataStore.getTodoItems()[0];
    const currentTime = new Date().getTime();

    todoDataStore.toggleComplete(todoItem);

    expect(todoItem.completed).toEqual(new Date().getTime());
});

test("Toggling the complete status of an item should not affect any other todo items", () => {
    todoDataStore.clearTodoItems();
    todoDataStore.addTodoItem("This item should not be marked as complete");
    todoDataStore.addTodoItem("This item should be marked as complete");
    todoDataStore.addTodoItem("Another item that should not be marked as complete");

    const todoItems = todoDataStore.getTodoItems();
    const relevantTodoItem = todoItems[1];
    todoDataStore.toggleComplete(relevantTodoItem);

    todoDataStore.getTodoItems().map((todoItem) => {
        if (todoItem.index === relevantTodoItem.index) {
            expect(todoItem.completed).toBe(new Date().getTime());
            return;
        }

        expect(todoItem.completed).toBe(false);
    });

    todoDataStore.toggleComplete(relevantTodoItem);

    todoDataStore.getTodoItems().map((todoItem) => {
        expect(todoItem.completed).toBe(false);
    });
});

test("You should be able to change the contents of todo action without modifying the rest of the todo items", () => {
    todoDataStore.clearTodoItems();

    todoDataStore.addTodoItem("This item will not have it's action changed");
    todoDataStore.addTodoItem("This item will not have it's action changed");
    todoDataStore.addTodoItem("This item will have it's action changed");
    todoDataStore.addTodoItem("This item will not have it's action changed");

    const updateTodoItem = todoDataStore.getTodoItems()[1];

    todoDataStore.updateTodoItem(updateTodoItem, "This is the updated action of the todo item");

    const updatedTodoItem = todoDataStore.getTodoItems()[1];

    expect(updatedTodoItem).toMatchObject({
        index: 3,
        action: "This is the updated action of the todo item",
        completed: false,
    });

    todoDataStore.toggleComplete(updatedTodoItem);

    expect(updatedTodoItem).toMatchObject({
        index: 3,
        action: "This is the updated action of the todo item",
        completed: new Date().getTime(),
    });

    expect(todoDataStore.getTodoItems()).toMatchObject([
        {
            action: "This item will not have it's action changed",
            completed: false,
            index: 4,
        },
        {
            action: "This is the updated action of the todo item",
            completed: new Date().getTime(),
            index: 3,
        },
        {
            action: "This item will not have it's action changed",
            completed: false,
            index: 2,
        },
        {
            action: "This item will not have it's action changed",
            completed: false,
            index: 1,
        },
    ]);
});

test("Items without a blank action item shouldn't be saved", () => {
    const emptyDataStore = new TodoDataStore();

    emptyDataStore.addTodoItem("");

    expect(emptyDataStore.getTodoItems()).toMatchObject([]);
});

test("Existing items can not be edited to be empty", () => {
    const editingDataStore = new TodoDataStore();

    editingDataStore.addTodoItem("This is the item");

    const editTodoItems = editingDataStore.getTodoItems();

    editingDataStore.updateTodoItem(editTodoItems[0], "");

    expect(editingDataStore.getTodoItems()).toMatchObject(editTodoItems);
});

jest.clearAllMocks();
