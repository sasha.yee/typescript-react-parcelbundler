/// <reference types="jest" />

/* the following should be tested:
1. AddTodo.tsx
[x] Snapshot
[x] Functional
2. TodoApp.tsx
[x] Snapshot
[n] Functional
*/

import * as Enzyme from "enzyme";
import * as EnzymeAdapter from "enzyme-adapter-react-16";
import * as React from "react";
import * as ReactDOM from "react-dom";
import * as ReactTestRenderer from "react-test-renderer";

import AddTodo from "Todo/AddTodo";
import TodoApp from "Todo/TodoApp";

test("The Add Todo Component should match the snapshot", () => {
    const component = ReactTestRenderer.create(<AddTodo addTodoItem={jest.fn()} />);

    expect(component.toJSON()).toMatchSnapshot();
});

test("Submitting the form should return the data entered into the input field", () => {
    Enzyme.configure({ adapter: new EnzymeAdapter() });

    const inputData = "This is the contents of the input";

    const formCallback = jest.fn((inputContents) => inputContents);

    const component = Enzyme.mount(<AddTodo addTodoItem={formCallback} />);

    const form = component.find("form");
    const input = component.find("input");

    expect(form.exists()).toBe(true);
    expect(input.exists()).toBe(true);

    input.simulate("change", { target: { value: inputData } });

    form.simulate("submit", { preventDefault: jest.fn() });

    expect(formCallback).toBeCalledWith(inputData);
});

test("The user should not be able to create an empty todo item", () => {
    Enzyme.configure({ adapter: new EnzymeAdapter() });

    const inputData = "";

    const formCallback = jest.fn((inputContents) => inputContents);

    const component = Enzyme.mount(<AddTodo addTodoItem={formCallback} />);

    const form = component.find("form");
    const input = component.find("input");

    expect(form.exists()).toBe(true);
    expect(input.exists()).toBe(true);

    input.simulate("change", { target: { value: inputData } });

    form.simulate("submit", { preventDefault: jest.fn() });

    const errorMessage = component.find(".todo-error-message");
    expect(errorMessage.exists()).toBe(true);

    expect(formCallback).not.toBeCalledWith(inputData);
});

test("The user should not be able to create a todo item that consists on only whitespace", () => {
    Enzyme.configure({ adapter: new EnzymeAdapter() });

    const inputData = "          ";

    /* const exoticWhitespace = "	              ​    　 "; */

    const formCallback = jest.fn((inputContents) => inputContents);

    const component = Enzyme.mount(<AddTodo addTodoItem={formCallback} />);

    const form = component.find("form");
    const input = component.find("input");

    expect(form.exists()).toBe(true);
    expect(input.exists()).toBe(true);

    input.simulate("change", { target: { value: inputData } });

    form.simulate("submit", { preventDefault: jest.fn() });

    const errorMessage = component.find(".todo-error-message");
    expect(errorMessage.exists()).toBe(true);

    expect(formCallback).not.toBeCalledWith(inputData);
});

test("The error message should be cleared after the user enters text into the text input", () => {
    Enzyme.configure({ adapter: new EnzymeAdapter() });

    const inputData = "";

    const formCallback = jest.fn((inputContents) => inputContents);

    const component = Enzyme.mount(<AddTodo addTodoItem={formCallback} />);

    const form = component.find("form");
    const input = component.find("input");

    input.simulate("change", { target: { value: inputData } });

    form.simulate("submit", { preventDefault: jest.fn() });

    const errorMessage = component.find(".todo-error-message");
    expect(errorMessage.exists()).toBe(true);

    input.simulate("change", { target: { value: "New value" } });
    const clearedErrorMessage = component.find(".todo-error-message");
    expect(clearedErrorMessage.exists()).toBe(false);
});

test("The Todo App should render and match the snapshot", () => {
    const component = ReactTestRenderer.create(<TodoApp />);

    expect(component.toJSON()).toMatchSnapshot();
});
