import * as Enzyme from "enzyme";
import * as EnzymeAdapter from "enzyme-adapter-react-16";
import * as React from "react";
import * as ReactDOM from "react-dom";
import * as ReactTestRenderer from "react-test-renderer";

import AddTodo from "Todo/AddTodo";
import TodoApp from "Todo/TodoApp";
import TodoItemComponent from "Todo/TodoItemComponent";

test("The user should not be able to edit a todo item to only contain whitespace", () => {
    Enzyme.configure({ adapter: new EnzymeAdapter() });

    const inputData = "          ";

    const updateTodoItem = jest.fn((updatedTodoAction) => updatedTodoAction);

    const todoItem = {
        index: 1,
        action: "This is an action",
        completed: false,
    };

    const component = Enzyme.mount(
        <TodoItemComponent todoItem={todoItem} updateItem={updateTodoItem} toggleComplete={jest.fn()} />,
    );

    expect(component).toMatchSnapshot();

    const input = component.find("input[type='text']");

    expect(input.exists()).toBe(true);

    input.simulate("change", { target: { value: inputData } });

    const errorMessage = component.find(".todo-error-message");
    expect(errorMessage.exists()).toBe(true);
});
