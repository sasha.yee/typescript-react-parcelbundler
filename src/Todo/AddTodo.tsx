import * as React from "react";
import { TodoError } from "./TodoApp";

import "./AddTodo.css";

interface AddTodoProps {
    addTodoItem: (todoAction: string) => void;
}

interface AddTodoState {
    todoAction: string;
    error?: TodoError;
}

export default class AddTodo extends React.Component<AddTodoProps, AddTodoState> {
    private input!: HTMLInputElement;

    constructor(props: AddTodoProps) {
        super(props);

        this.state = {
            todoAction: "",
        };
    }

    private inputRef(input: HTMLInputElement) {
        this.input = input;
    }

    private onUpdateInput(event: React.ChangeEvent<HTMLInputElement>) {
        this.setState({ todoAction: event.target.value, error: undefined });
    }

    private onSubmit(event: React.FormEvent<HTMLFormElement>) {
        event.preventDefault();

        const action = this.state.todoAction.replace(/^\s+/, "");

        if (action.length === 0) {
            this.setState({ error: { errorMessage: "Please add an action that needs to be completed" } });
            return;
        }

        this.props.addTodoItem(action);
        this.input.value = "";
        this.setState({ todoAction: "" });
    }

    public render(): React.ReactElement<any> {
        return (
            <form onSubmit={this.onSubmit.bind(this)} key="addTodoForm" className="add-todo">
                <input
                    name="todo-action"
                    ref={this.inputRef.bind(this)}
                    onChange={this.onUpdateInput.bind(this)}
                    value={this.state.todoAction}
                    id="todo-add-action"
                />
                <button type="submit" title="Add todo item">
                    +
                </button>
                {this.renderErrorMessage()}
            </form>
        );
    }

    private renderErrorMessage() {
        const { error } = this.state;

        if (!error) {
            return;
        }

        return <div className="todo-error-message">{error.errorMessage}</div>;
    }
}
