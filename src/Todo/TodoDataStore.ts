export interface TodoItem {
    index: number;
    action: string;
    completed: number | false;
}

const STORAGE_KEY = "todo-data";

// todo add appropriate method signatures
export default class TodoDataStore {
    private todoItems: TodoItem[];

    constructor() {
        this.todoItems = this.retrieveStoredData();
    }

    public getTodoItems(): TodoItem[] {
        return this.todoItems;
    }

    public addTodoItem(action: string): TodoItem[] {
        if (action && action.length > 0) {
            const newTodoItem = this.createTodoItem(action);
            const newList = this.todoItems.slice();
            newList.unshift(newTodoItem);
            this.todoItems = newList;

            this.storeData();
        }

        return this.todoItems;
    }

    private createTodoItem(action: string): TodoItem {
        return {
            index: this.todoItems.length + 1,
            action,
            completed: false,
        };
    }

    public toggleComplete(todoItem: TodoItem): TodoItem[] {
        if (todoItem.completed) {
            todoItem.completed = false;
        } else {
            todoItem.completed = new Date().getTime();
        }

        const todoItems = this.todoItems.map((originalItem) => {
            if (originalItem.index !== todoItem.index) {
                return originalItem;
            }

            return todoItem;
        });

        this.storeData();

        return todoItems;
    }

    public updateTodoItem(updateTodoItem: TodoItem, newAction: string) {
        updateTodoItem.action = newAction;

        const todoItems = this.todoItems.map((originalItem) => {
            if (originalItem.index !== updateTodoItem.index) {
                return originalItem;
            }

            return updateTodoItem;
        });

        this.storeData();

        return todoItems;
    }

    public clearTodoItems(): void {
        this.todoItems = [];

        this.storeData();
    }

    private storeData(): void {
        localStorage.setItem(STORAGE_KEY, JSON.stringify(this.todoItems));
    }

    private retrieveStoredData(): TodoItem[] {
        const data = localStorage.getItem(STORAGE_KEY);
        return (data && JSON.parse(data)) || [];
    }
}
