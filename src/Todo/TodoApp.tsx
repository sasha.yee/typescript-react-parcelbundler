import * as React from "react";
import AddTodo from "./AddTodo";
import TodoDataStore, { TodoItem } from "./TodoDataStore";
import TodoItemComponent from "./TodoItemComponent";

import "./TodoApp.css";

interface TodoAppState {
    todoItems: TodoItem[];
}

export interface TodoError {
    errorMessage: string;
}

export default class TodoApp extends React.Component<any, TodoAppState> {
    private todoDataStore = new TodoDataStore();

    constructor() {
        super(undefined);

        this.state = {
            todoItems: this.todoDataStore.getTodoItems(),
        };
    }

    private addTodoItem(action: string): void {
        const todoItems = this.todoDataStore.addTodoItem(action);
        this.setState({ todoItems });
    }

    private toggleComplete(todoItem: TodoItem): void {
        const todoItems = this.todoDataStore.toggleComplete(todoItem);

        this.setState({ todoItems });
    }

    private updateTodoItem(todoItem: TodoItem, newAction: string) {
        const todoItems = this.todoDataStore.updateTodoItem(todoItem, newAction);

        this.setState({ todoItems });
    }

    public render(): React.ReactElement<any> {
        return (
            <div className="todo-app">
                <AddTodo addTodoItem={this.addTodoItem.bind(this)} />
                {this.renderTodoItems()}
            </div>
        );
    }

    private renderTodoItems(): Array<React.ReactElement<any>> {
        return this.state.todoItems.map((todoItem) => {
            return (
                <TodoItemComponent
                    todoItem={todoItem}
                    toggleComplete={() => {
                        this.toggleComplete(todoItem);
                    }}
                    updateItem={(newAction) => {
                        this.updateTodoItem(todoItem, newAction);
                    }}
                    key={todoItem.index}
                />
            );
        });
    }
}
