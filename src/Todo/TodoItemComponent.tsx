import * as React from "react";
import { TodoError } from "./TodoApp";
import { TodoItem } from "./TodoDataStore";

interface TodoItemComponentProps {
    todoItem: TodoItem;
    toggleComplete?: () => void;
    updateItem: (newAction: string) => void;
}

interface TodoItemComponentState {
    disabled: boolean;
    action: string;
}

export default class TodoItemComponent extends React.Component<TodoItemComponentProps, TodoItemComponentState> {
    constructor(props: TodoItemComponentProps) {
        super(props);

        this.state = {
            disabled: true,
            action: props.todoItem.action,
        };
    }

    public componentWillReceiveProps(newProps: TodoItemComponentProps, newState: TodoItemComponentState) {
        if (this.props.todoItem !== newProps.todoItem) {
            this.setState({
                action: newProps.todoItem.action,
            });
        }
    }

    public editOnChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const action = event.target.value.replace(/^\s+/, "");

        this.setState(
            {
                action,
            },
            () => {
                if (action) {
                    this.props.updateItem(action);
                }
            },
        );
    };

    public render() {
        const { todoItem, toggleComplete, updateItem } = this.props;
        const { disabled, action } = this.state;

        const completed = todoItem.completed && todoItem.completed > 0 ? true : false;
        const classes = ["todo-item", completed ? "complete" : ""];

        // action || '' is required to prevent the component from changing from
        // a controlled to an uncontrolled input.
        return (
            <div className={classes.filter((c) => c).join(" ")}>
                <input type="checkbox" onChange={toggleComplete} checked={completed} />
                <div className="todo-item-text">
                    <input type="text" value={action || ""} onChange={this.editOnChange} />
                </div>
                {!action && this.renderError()}
            </div>
        );
    }

    public renderError() {
        return <div className="todo-error-message">The action can not be empty</div>;
    }

    public renderContextualActions() {
        return;
    }
}
