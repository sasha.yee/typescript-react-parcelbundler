class LocalStorageMock {
    private store: any;

    constructor() {
        this.store = {};
    }

    public clear(): void {
        this.store = {};
    }

    public getItem(key: string): string | undefined {
        return this.store[key] || undefined;
    }

    public setItem(key: string, value: string): void {
        this.store[key] = value.toString();
    }

    public removeItem(key: string): void {
        delete this.store[key];
    }
}

(global as any).localStorage = new LocalStorageMock();
