import * as React from "react";

export interface HeaderProps {
    menuItems: MenuItem[];
}

export interface MenuItem {
    title: string;
    link: string;
    subMenuItems?: MenuItem[];
}

export default class SampleHeader extends React.Component<HeaderProps, any> {
    public render() {
        const { menuItems } = this.props;
        return (
            <nav>
                <ul>
                    {menuItems.map((menuItem, index) => (
                        <HeaderItem {...menuItem} key={index} />
                    ))}
                </ul>
            </nav>
        );
    }
}

const HeaderItem: React.SFC<MenuItem> = (props) => {
    const { title, link, subMenuItems } = props;
    return (
        <li>
            <a href={link}>{title}</a>
            {subMenuItems && renderSubMenuItems(subMenuItems)}
        </li>
    );
};

const renderSubMenuItems: React.SFC<MenuItem[]> = (
    subMenuItems: MenuItem[],
) => {
    return (
        <ul>
            {subMenuItems.map((subMenuItem, index) => (
                <HeaderItem {...subMenuItem} key={index} />
            ))}
        </ul>
    );
};
