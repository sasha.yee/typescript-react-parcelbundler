/// <reference types="jest" />

import * as React from "react";
import * as ReactDOM from "react-dom";
import * as ReactTestRenderer from "react-test-renderer";

import Header, { HeaderProps, MenuItem } from "components/SampleHeader";

const headerProps: HeaderProps = {
    menuItems: [
        {
            title: "Hello There!",
            link: "/hello-there",
        },
        {
            title: "This has submenu items",
            link: "/top-level-menu",
            subMenuItems: [
                {
                    title: "Submenu one",
                    link: "/submenu-one",
                },
                {
                    title: "Submenu two",
                    link: "/submenu-two",
                },
                {
                    title: "Submenu three",
                    link: "/submenu-three",
                    subMenuItems: [
                        {
                            title: "Subsubmenu one",
                            link: "/submenu-three/subsubmenu-one",
                        },
                    ],
                },
            ],
        },
    ],
};

test("The header renders", () => {
    const div = document.createElement("div");
    ReactDOM.render(<Header {...headerProps} />, div);
});

test("The header matches the snapshot", () => {
    const component = ReactTestRenderer.create(<Header {...headerProps} />);
    expect(component.toJSON()).toMatchSnapshot();
});

test("The header has the right number of components", () => {
    const component = ReactTestRenderer.create(<Header {...headerProps} />);
    const anchorElements = component.root.findAllByType("a");

    const menuItems = headerProps.menuItems.map((a) => ({ ...a }));
    const flattenedMenu = flattenMenu(menuItems);

    expect(anchorElements.length).toEqual(flattenedMenu.length);
});

function flattenMenu(menuItems: MenuItem[]): MenuItem[] {
    let flattened: MenuItem[] = [];

    if (menuItems) {
        for (const menuItem of menuItems) {
            const subMenuItems = menuItem.subMenuItems && menuItem.subMenuItems.map((a) => ({ ...a }));

            delete menuItem.subMenuItems;

            flattened = flattened.concat(menuItem);

            if (subMenuItems && subMenuItems.length > 0) {
                flattened = flattened.concat(flattenMenu(subMenuItems));
            }
        }
    }

    return flattened;
}

test("menuItemCount should flatten an array correctly: 0", () => {
    expect(flattenMenu([])).toEqual([]);
});

test("flattenMenu should flatten an array correctly: 1", () => {
    const mockMenuItem: MenuItem[] = [
        {
            title: "Test",
            link: "/test",
        },
    ];

    expect(flattenMenu(mockMenuItem)).toEqual(mockMenuItem);
});

test("flattenMenu should flatten an array correctly: 2", () => {
    const mockMenuItem: MenuItem[] = [
        {
            title: "Test",
            link: "/test",
        },
        {
            title: "Test 2",
            link: "/test-2",
        },
    ];

    const expectedFlattenedMenu = [
        {
            title: "Test",
            link: "/test",
        },
        {
            title: "Test 2",
            link: "/test-2",
        },
    ];

    const flattenedMenu = flattenMenu(mockMenuItem);

    expect(flattenedMenu).toEqual(expectedFlattenedMenu);
});

test("flattenMenu should flatten an array correctly: 3", () => {
    const mockMenuItem: MenuItem[] = [
        {
            title: "Test",
            link: "/test",
        },
        {
            title: "Test 2",
            link: "/test-2",
            subMenuItems: [
                {
                    title: "Test 3",
                    link: "/test-3",
                },
                {
                    title: "Test 4",
                    link: "/test-4",
                },
            ],
        },
    ];

    const expectedFlattenedMenu: MenuItem[] = [
        {
            title: "Test",
            link: "/test",
        },
        {
            title: "Test 2",
            link: "/test-2",
        },
        {
            title: "Test 3",
            link: "/test-3",
        },
        {
            title: "Test 4",
            link: "/test-4",
        },
    ];

    const flattened = flattenMenu(mockMenuItem);

    expect(flattened).toEqual(expectedFlattenedMenu);
});

test("menuItemCount should flatten an array correctly: 4", () => {
    const expectedFlattenedMenu: MenuItem[] = [
        {
            title: "Hello There!",
            link: "/hello-there",
        },
        {
            title: "This has submenu items",
            link: "/top-level-menu",
        },
        {
            title: "Submenu one",
            link: "/submenu-one",
        },
        {
            title: "Submenu two",
            link: "/submenu-two",
        },
        {
            title: "Submenu three",
            link: "/submenu-three",
        },
        {
            title: "Subsubmenu one",
            link: "/submenu-three/subsubmenu-one",
        },
    ];

    const menuItems = headerProps.menuItems.map((a) => ({ ...a }));
    const flattened = flattenMenu(menuItems);

    expect(flattened).toEqual(expectedFlattenedMenu);
});
