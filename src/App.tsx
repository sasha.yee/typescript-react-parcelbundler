import * as React from "react";
import TodoApp from "Todo/TodoApp";

export default class App extends React.Component<any, any> {
    public render() {
        return (
            <div>
                <TodoApp />
            </div>
        );
    }
}
